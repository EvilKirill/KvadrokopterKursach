%% Константы и параметры модели

b = 4.27 * 10^(-6);     % H/(рад/c)^2
d = 0.6 * 10^(-6);      % H*м/(рад/c)^2

Jx = 0.01;              % кг*м^2
Jy = 0.01;              % кг*м^2
Jz = 0.03;              % кг*м^2
m = 1.3;                % кг
l = 0.25;               % м

g = 9.8;                % м/с^2

%% Линеаризованная математическая модель

A1 = [0 1;              % z
      0 0];             % dz

A2 = [0 1 0 0 0 0;      % phi
      0 0 0 0 0 0;      % dphi
      0 0 0 1 0 0;      % theta
      0 0 0 0 0 0;      % dtheta
      0 0 0 0 0 1;      % psi
      0 0 0 0 0 0];     % dpsi

%     U1                (сила)
B1 = [0;
      1];

%       U2    U3    U4  (моменты)
B2 = [  0     0     0;
       1/Jx   0     0;
        0     0     0;
        0    1/Jy   0;
        0     0     0;
        0     0    1/Jz];

C1 = eye(2);
C2 = eye(6);

D1 = zeros(2,1);
D2 = zeros(6,3);

% Совмещаем систему углов и высоты
A1(3:8,3:8) = A2;
B1(3:8,2:4) = B2;
C1(3:8,3:8) = C2;
D1(3:8,2:4) = D2;

%% ПИД-регулятор

% Расчет выполнен методом Циглера-Николса

% Полученные параметры
Ku = 45;
DT = 1.88;

% Рассчет П И Д коэффициентов
Kp = 0.6*Ku
Ki = (2*Kp)/DT
Kd = (Kp*DT)/8

% После калибровки
Kp = 27;
Ki = 12.3;
Kd = 13.4;

% Для моделирования ПИД-регулятора нужно в схеме в полетном контроллере
% заменить матричный гейн на блок с ПИДами, поскольку при использовании
% свитча модель почему-то работает некорректно

%% Модальный регулятор

G = [-1  0  0  0  0  0  0  0;
      0 -2  0  0  0  0  0  0;
      0  0 -1  0  0  0  0  0;
      0  0  0 -2  0  0  0  0;
      0  0  0  0 -1  0  0  0;
      0  0  0  0  0 -2  0  0;
      0  0  0  0  0  0 -1  0;
      0  0  0  0  0  0  0 -2]; 

Y = [ 2  1  0  0  0  0  0  0;
      0  0  2  1  0  0  0  0;
      0  0  0  0  2  1  0  0;
      0  0  0  0  0  0  2  1];

cvx_begin sdp
variable P(8,8)
A1*P-P*G == B1*Y;
cvx_end
K = Y*inv(P)

eig(A1-B1*K)

%% LMI-регулятор с заданной степенью устойчивости

a = 1; % желаемая степень устойчивости

cvx_begin sdp
variable P(8,8) symmetric
variable Y(4,8)
P > 0.000001*eye(8);
P*A1' + A1*P + 2*a*P + Y'*B1' + B1*Y <= 0;
cvx_end
K = Y*inv(P);

K = round(-K,4)
eig(A1-B1*K)

%% LMI-регулятор с минимизацией управления

x0 = [0; 0; 0; 0; 0; 0; 0; 0]; % начальные условия
a = 1; % желаемая степень устойчивости

cvx_begin sdp
variable P(8,8) symmetric
variable Y(4,8)
variable mumu
minimize mumu
P > 0.000001*eye(8);
P*A1' + A1*P + 2*a*P + Y'*B1' + B1*Y <= 0;
[P x0;
x0' 1] > 0;
[P Y';
 Y mumu*eye(4)] > 0;
cvx_end
K = Y*inv(P);

K = round(-K,4)
eig(A1-B1*K)
sqrt(mumu)

%% LQ-регулятор

Q = [30 0 0 0 0 0 0 0;
     0 30 0 0 0 0 0 0;
     0  0 5 0 0 0 0 0;
     0  0 0 5 0 0 0 0;
     0  0 0 0 5 0 0 0;
     0  0 0 0 0 5 0 0;
     0  0 0 0 0 0 1 0;
     0  0 0 0 0 0 0 1];

R = [1 0 0 0;
     0 1 0 0;
     0 0 1 0;
     0 0 0 1];

[K,S,e] = lqr(A1,B1,Q,R);

K = round(K,4)
eig(A1-B1*K)

%% H2-регулятор

Cz = [1 0 0 0 0 0 0 0;
      0 0 1 0 0 0 0 0;
      0 0 0 0 1 0 0 0;
      0 0 0 0 0 0 1 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0];

Dz = [0  0    0    0;
      0  0    0    0;
      0  0    0    0;
      0  0    0    0;
      1  0    0    0;
      0 1/Jx  0    0;
      0  0   1/Jy  0;
      0  0    0   1/Jz];

[K,S,e] = lqr(A1,B1,Cz'*Cz,Dz'*Dz);

K = round(K,4)
eig(A1-B1*K)

% АЧХ замкнутой системы по координате Z
% sys = ss(A1-B1*K,[0 1 0 0 0 0 0 0]',[1 0 0 0 0 0 0 0],0);
% sys = tf(sys);
% margin(sys)

%% Hinf-регулятор

Cz = [1 0 0 0 0 0 0 0;
      0 0 1 0 0 0 0 0;
      0 0 0 0 1 0 0 0;
      0 0 0 0 0 0 1 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0;
      0 0 0 0 0 0 0 0];

Dz = [0  0    0    0;
      0  0    0    0;
      0  0    0    0;
      0  0    0    0;
      1  0    0    0;
      0 1/Jx  0    0;
      0  0   1/Jy  0;
      0  0    0   1/Jz];

Bw = [0 0;
      1 0;
      0 0;
      1 0;
      0 0;
      1 0;
      0 0;
      1 0];

gamma = 2.1;
[X,K_,L] = icare(A1,B1,Cz'*Cz,Dz'*Dz,zeros(8,4),eye(8),gamma^(-2)*(Bw * Bw'));

K = [K_(1,1) K_(1,2)     0       0       0       0       0       0   ;
        0       0     K_(2,3) K_(2,4)    0       0       0       0   ;
        0       0        0       0    K_(3,5) K_(3,6)    0       0   ;
        0       0        0       0       0       0    K_(4,7) K_(4,8)]
eig(A1-B1*K)

% АЧХ замкнутой системы по координате Z
% sys = ss(A1-B1*K,[0 1 0 0 0 0 0 0]',[1 0 0 0 0 0 0 0],0);
% sys = tf(sys);
% margin(sys)

%% Флаги для моделирования

sw = 0;                 % Возмущения ВЫКЛ(0)/ВКЛ(1)
sinAMP = 0.1;           % Амплитуда возмущений
sinFREQ = 1;            % Частота возмущений
sinBIAS = 0;            % Смещение возмущений

spiral_trj_flag = 1;    % Полет по спирали (рекомендую включать для визуализации) ВЫКЛ(0)/ВКЛ(1)

% Эти коэффициенты нужно настраивать, если хотим не LQR во внутреннем каскаде
P_cascade = -5;         % Параметры ПИДа внешнего каскада (подобраны для внутреннего LQR)
I_cascade = -2;         % Параметры ПИДа внешнего каскада (подобраны для внутреннего LQR)
D_cascade = -20;        % Параметры ПИДа внешнего каскада (подобраны для внутреннего LQR)

%% Графики

% Здесь можно построить графики для различных регуляторов. Просто запускаем
% блок с расчетом нужного нам регулятора, далее меняем флаги выше, если
% необходимо. 

% Визуализацию полета при снятии характеристик рекомендуется закомментить в
% схеме, так как смысла она не несет (более адекватного способа ее
% отключать мы не нашли)

SimOut = sim('Project_Nonlinear_Model.slx', 'ReturnWorkspaceOutputs', 'on');
% 'Project_Nonlinear_Model.slx'

if sw == 0
t = SimOut.Control(:, 1);
Thrust = SimOut.Control(:, 2);
M1 = SimOut.Control(:, 3);
M2 = SimOut.Control(:, 4);
M3 = SimOut.Control(:, 5);
h1 = figure;
    set(h1, 'DefaultAxesFontSize', 14, 'DefaultAxesFontName', 'Times New Roman');
    plot (t, Thrust, 'LineWidth', 1.5)
    grid on
    grid minor
    ylabel('\itU_1, H')
    xlabel('\itt, с')
trapz(t,Thrust)
end

t = SimOut.LinearMovements(:, 1);
Z = SimOut.LinearMovements(:, 4);
h2 = figure;
    set(h2, 'DefaultAxesFontSize', 14, 'DefaultAxesFontName', 'Times New Roman');
    plot (t, Z, 'LineWidth', 1.5)
    hold on
    grid on
    grid minor
    ylabel('\itZ, м')
    xlabel('\itt, с')


t = SimOut.RotationalMovements(:, 1);
Roll = SimOut.RotationalMovements(:, 2);
Pitch = SimOut.RotationalMovements(:, 3);
Yaw = SimOut.RotationalMovements(:, 4);
h3 = figure;
    set(h3, 'DefaultAxesFontSize', 14, 'DefaultAxesFontName', 'Times New Roman');
    plot (t, Roll, 'LineWidth', 1.5)
    grid on
    grid minor
    hold on
    plot (t, Pitch, 'LineWidth', 1.5)
    hold on
    plot (t, Yaw, 'LineWidth', 1.5)
    ylabel('\itрад')
    xlabel('\itt, с')
    legend('Крен','Тангаж','Рысканье')

%% Время переходного процесса, перерегулирование, СКО

% Если моделирование проводилось без возмущений - считаем время переходного
% процесса и перерегулирование; если с возмущениями - СКО

% По высоте
if sw == 0
    Zn = Z(1);
    Zss = Z(end);
    Z0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Zss-Zn);
    ttr5 = max(t(abs(Z-Zss)>D5))-t0
    dy = (max(Z)-Zss)/abs(Zss-Z0)*100
end
if sw == 1
    Zn = Z(1);
    Zss = Z(end);
    Z0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Zss-Zn);
    ttr5 = max(t(abs(Z-Zss)>D5))-t0;
    ind = t > ttr5;
    des = 1;
    errZ = des - Z(ind);
    CKO = std(errZ)
end

% По углу Крена
if sw == 0
    Rolln = Roll(1);
    Rollss = Roll(end);
    Roll0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Rollss-Rolln);
    ttr5 = max(t(abs(Roll-Rollss)>D5))-t0
    dy = (max(Roll)-Rollss)/abs(Rollss-Roll0)*100
end
if sw == 1
    Rolln = Roll(1);
    Rollss = Roll(end);
    Roll0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Rollss-Rolln);
    ttr5 = max(t(abs(Roll-Rollss)>D5))-t0;
    ind = t > ttr5;
    des = 1;
    errRoll = des - Roll(ind);
    CKO = std(errRoll)
end

% По углу Тангажа
if sw == 0
    Pitchn = Pitch(1);
    Pitchss = Pitch(end);
    Pitch0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Pitchss-Pitchn);
    ttr5 = max(t(abs(Pitch-Pitchss)>D5))-t0
    dy = (max(Pitch)-Pitchss)/abs(Pitchss-Pitch0)*100
end
if sw == 1
    Pitchn = Pitch(1);
    Pitchss = Pitch(end);
    Pitch0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Pitchss-Pitchn);
    ttr5 = max(t(abs(Pitch-Pitchss)>D5))-t0;
    ind = t > ttr5;
    des = 1;
    errPitch = des - Pitch(ind);
    CKO = std(errPitch)
end

% По углу Рысканья
if sw == 0
    Yawn = Yaw(1);
    Yawss = Yaw(end);
    Yaw0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Yawss-Yawn);
    ttr5 = max(t(abs(Yaw-Yawss)>D5))-t0
    dy = (max(Yaw)-Yawss)/abs(Yawss-Yaw0)*100
end
if sw == 1
    Yawn = Yaw(1);
    Yawss = Yaw(end);
    Yaw0 = 0;
    t0 = 0;
    D5 = 0.05*abs(Yawss-Yawn);
    ttr5 = max(t(abs(Yaw-Yawss)>D5))-t0;
    ind = t > ttr5;
    des = 1;
    errYaw = des - Yaw(ind);
    CKO = std(errYaw)
end

%% Внешний каскад управления по Х, Y графики

% Запускаем блок с расчетом LQ-регулятора, выставляем следующие флаги:
% spiral_trj_flag = 1;
% sw = 0;
% запускаем этот блок кода и наслаждаемся полетом по спирали

spiral_trj_flag = 1;
sw = 0;

if spiral_trj_flag == 1
    SimOut = sim('Project_Nonlinear_Model.slx', 'ReturnWorkspaceOutputs', 'on');

    t = SimOut.LinearMovements(:, 1);
    X = SimOut.LinearMovements(:, 2);
    Y = SimOut.LinearMovements(:, 3);
    Z = SimOut.LinearMovements(:, 4);
    h0 = figure;
        set(h0, 'DefaultAxesFontSize', 14, 'DefaultAxesFontName', 'Times New Roman');
        plot3(X,Y,Z)
        grid on
        grid minor
        zlabel('\itZ, м')
        ylabel('\itY, м')
        xlabel('\itX, м')
end
